



class persona:
    '''Es mi primera clase
    
    inputs: debes ingresar un string con el nombre de una persona
    
    Todas las variables que usen inicializarla
    '''

    def __init__(self,nombre,apellido,edad):
        
        self.nombre=nombre # Atributo publico
        self.apellido = apellido
        self.edad = edad # Atributo publico
        self._edadProt = 23 # Atributo protegido
        self.__informacionPrivada = ".." # Atributo privado

    
    def printName(self):
        '''Atributos publicos'''
        return self.nombre+' '+self.apellido
        #print("hola:{}, tienes {} años".format(self.nombre,self.edad))

    def informacion(self):
        print('{} tiene {} años '.format(self.printName(),self.edad))
    '''Un metodo que llame a otro método'''
    

# Clase hija de la clase persona, hereda atributos de la clase padre
class persona2(persona):
    def __init__(self, nombre, apellido, edad, estatura):
        '''Tiene los mismos atributos de persona'''
        super().__init__(nombre, apellido, edad) 
        '''Super inicializa todas las variables en la clase padre'''
        self.estatura = estatura
        '''Persona 2 tendrá '''

    def pestatura(self):
        print("Estatura: {}, edad:{}".format(self.estatura,self.edad))

    # se pueden redefinir los métodos
    def informacion(self):
        print('{} tiene {} años  y {} de estatura'.format(self.printName(),self.edad,self.estatura))
    '''Un metodo que llame a otro método'''
