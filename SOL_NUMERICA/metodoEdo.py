
import numpy as np

class solNumerica:

    def __init__(self,a,b,n,y0,f):
        self.a = a
        self.b = b
        self.n = n
        self.f = f

    def h(self):
        return (self.b-self.a)/self.n
    
    def X(self):
        self.x = np.arange(self.a,self.b+self.h(),self.h())
        
    def euler(self):
        self.X()


        #Tarea hacer el cálculo de Euler para todos los puntos de y_n

