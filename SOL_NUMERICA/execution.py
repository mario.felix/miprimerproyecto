from metodoEdo import solNumerica

if __name__=="__main__":
    print("Inicio de función para solucionar EDO")
    a = 1
    b = 2
    n = 10
    y0 = -3
    f = lambda x,y: -2*x+x*y
    sol_euler = solNumerica(a,b,n,y0,f)
    print("a:{}, b:{},h:{}".format(sol_euler.a,sol_euler.b,sol_euler.h()))
    sol_euler.X()

