import numpy as np
import matplotlib.pyplot as plt
class tiroParabolico():
    
    '''
    crear una clase que se llame tiro parabólico:
    -   inputs:
        *   angulo
        *   velocidad inicial, 
        *   altura inicial
        *   La gravedad
        *   posición en x,
        *   calcular la velocidad en y,
        *   velocidad en x
        *   tiempo de vuelo
    
    Definir tres metodos, velocidad en x, velocidad en y, tiempo de vuelo
    -   Outputs:
    '''
    def __init__(self, theta, v0, h0, x0, g):
        self.rapidez = v0 
        self.theta = theta*(np.pi/180) # Variable en grados
        self.y0 = h0
        self.x0 = x0
        self.gravedad= g

    def xVelocidad(self):
        return self.rapidez*np.cos(self.theta)
    
    def yVelocidad(self,t):
        return self.rapidez*np.sin(self.theta)-self.gravedad*t

    def tiempoVuelo(self):
        return (2*self.rapidez*np.sin(self.theta))/(self.gravedad)
    def grafica(self,t):
        x = self.x0 +self.rapidez* np.cos(self.theta)*t
        y = self.y0 +self.rapidez*np.sin(self.theta)*t -0.5*self.gravedad*(t**2) 
        fig, ax1 = plt.subplots(1,2)
        ax1[0].plot(t,y,label='y')
        ax1[0].set_ylabel('posición en y')
        ax1[0].set_xlabel('Tiempo (s)')
        ax1[0].grid()
        ax1[1].plot(t,x,label='x')
        ax1[1].set_xlabel('Tiempo (s)')
        ax1[1].set_ylabel('Posición en x')
        ax1[1].grid()
        plt.show()
    # Pendiente hacer un método de grafique la trayectoria
        
class tiromodificado(tiroParabolico):
    def __init__(self, theta, v0, h0, x0, g,ax):
        super().__init__(theta, v0, h0, x0, g)
        self.ax = ax
    
    def xposicionmodi(self,t):
        x = self.x0 +self.rapidez* np.cos(self.theta)*t +0.5*self.ax*(t**2)
        return x
    
    def SoCDE(r,t):
        x, y, z = r
        dxdt = y-x
        dydt = y+x 
        dzdt = 1/(x+y+z)
        return [dxdt,dydt,dzdt]

