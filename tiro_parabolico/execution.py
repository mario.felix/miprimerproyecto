from personas import persona
from tiro_parabolico import tiroParabolico
import numpy as np
import matplotlib.pyplot as plt
'''Ejecutar un archivo desde un execution'''

if __name__=="__main__":
    '''
    DOCSTRING mi primera clase
    1. Claro con las variables, poner comentarios
    inputs:
        - Nombre: debes ingresar un string con el nombre de una persona

    '''
    nombre="Jose" # La variable debe ser un string
    apellido='Felix'
    edad =25
    personone = persona(nombre,apellido,edad) # Llamar la clase (Instanciarla)
    #print(person.__doc__) # Mostrar el docstring de la clase
    edaddos=24
    persondos = persona(nombre,apellido,edaddos)
    #personone.informacion()
    #persondos.informacion()
    theta = 30
    v0 = 30
    h0 = 20
    x0 = 1
    g = -9.81
    particula1 = tiroParabolico(theta,v0,h0,x0,g)
    t = np.linspace(0,10,4)
    tiempo = particula1.tiempoVuelo()
    xvelocidad = particula1.xVelocidad()
    yvelocidad = particula1.yVelocidad(t)
    print('El tiempo de vuelo es {}, la velocidad en x es {} y la velocidad en y es: {}'.format(tiempo, xvelocidad, yvelocidad))
    print(particula1.grafica(t))
